from odoo import models, fields, api, _
from odoo.exceptions import UserError


class KpiReportWizard(models.TransientModel):
    _name = 'kpi.report.status.wizard'

    region = fields.Many2one('kpi.regions')
    city = fields.Many2one('kpi.city', domain="[('region', '=', region)]")
    year = fields.Many2one('kpi.year')
    kpi = fields.Many2one('kpi.kpi')

    @api.multi
    def print_kpi_status_report(self):

        domains = []
        if self.region:
            domains.append(('region', '=', self.region.id))
        if self.city:
            domains.append(('city', '=', self.city.id))
        if self.year:
            domains.append(('year', '=', self.year))

        kpis = self.env['kpi.status'].search(domains).parameters.filtered(lambda x: x.kpi == self.kpi)

        if kpis.ids:
            return self.env.ref('kpi.action_kpi_status_report').report_action(kpis)
        else:
            raise UserError(_('No result found!'))

    @api.multi
    def print_kpi_status_parameter_report(self):
        domains = []

        if self.region:
            domains.append(('kpi_status_id.region', '=', self.region.id))
        if self.city:
            domains.append(('kpi_status_id.city', '=', self.city.id))
        if self.year:
            domains.append(('kpi_status_id.year', '=', self.year))
        if self.kpi:
            domains.append(('kpi', '=', self.kpi.id))

        kpis = self.env['kpi.status.parameter'].search(domains)

        if kpis.ids:
            return self.env.ref('kpi.action_kpi_status_parameter_report').report_action(kpis)
        else:
            raise UserError(_('No result found!'))
