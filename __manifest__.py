# -*- coding: utf-8 -*-
{
    'name': "KPI",

    'summary': """
        KPI manager for water works...""",

    'author': "Binyam Assefa, Musse Tesfaye",
    'website': "http://www.afomsoft.com",

    'category': 'Uncategorized',
    'version': '0.1',
    'application': True,

    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'report/paper_format.xml',
        'views/kpi.xml',
        'views/theme.xml',
        'views/region.xml',
        'views/year.xml',
        'views/report.xml',
        'report/kpi_report.xml',
        'views/menu.xml'

    ],
}
