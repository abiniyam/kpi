from odoo import models, fields, api


class Region(models.Model):
    _name = 'kpi.regions'

    name = fields.Char('Name', required=True)
    status = fields.Selection([('active', 'Active'), ('closed', 'Closed')], required=True, default='active')


class City(models.Model):
    _name = 'kpi.city'

    name = fields.Char('Name', required=True)
    region = fields.Many2one('kpi.regions', required=True)
    level = fields.Selection([(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)])
    remark = fields.Char('Remark')
    status = fields.Selection([('active', 'Active'), ('closed', 'Closed')], required=True, default='active')
