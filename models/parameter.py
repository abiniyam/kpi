from odoo import models, fields, api


class parameter(models.Model):
    _name = 'kpi.parameter'
    id = fields.Char('ID')
    name = fields.Char('Name', required=True)
    unit = fields.Char('Unit Of Measure', required=False)
    kpi = fields.Many2one('kpi.kpi', 'Parameters')
