from odoo import models, fields, api


class theme(models.Model):
    _name = 'kpi.theme'
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')])
    description = fields.Char('Description')
    name = fields.Char('Name', required=True)

