from odoo import models, fields, api


class Year(models.Model):
    _name = 'kpi.year'

    year = fields.Char('Year', required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], required=True, default='Active')

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            val = str(record.year)
            res.append((record.id, val))
        return res
