from odoo import models, fields, api, _
from odoo.exceptions import UserError


class kpi(models.Model):
    _name = 'kpi.kpi'
    name = fields.Char('Name', required=True)
    description = fields.Text('Description', required=True)
    unit_of_measure = fields.Char('Unit Of Measure', required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], required=True)
    parameters = fields.One2many('kpi.parameter', 'kpi')
    theme = fields.Many2one('kpi.theme', 'Theme', required=True)
    formula = fields.Text('Formula')


class kpistatus(models.Model):
    _name = 'kpi.status'

    region = fields.Many2one('kpi.regions', required=True)
    city = fields.Many2one('kpi.city', required=True, domain="[('region', '=', region)]")
    year = fields.Many2one('kpi.year', required=True, domain="[('status', '=', 'Active')]")
    parameters = fields.One2many('kpi.status.parameter', 'kpi_status_id')
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed'), ('Terminated', 'Terminated')], required=True,
                              default='Active')

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            val = str(record.region.name) + "  / " + str(record.city.name)
            res.append((record.id, val))
        return res

    @api.model
    def create(self, vals):

        # check if they city record is already created
        if self.check_region_city(vals['region'], vals['city'], vals['year']):
            raise UserError(_('City record is already created!'))

        res = super(kpistatus, self).create(vals)

        self.load_parameters(res.id)

        return res

    def write(self, vals):

        res = super(kpistatus, self).write(vals)

        kpi = 0
        for p in self.parameters:
            if kpi != p.kpi:
                kpi = p.kpi

                # Get Formula For KPI
                self.calculate_result(kpi,
                                      self.parameters.search([('kpi', '=', kpi.id), ('kpi_status_id', '=', self.id)]))

        return res

    def calculate_result(self, kpi, parameters):
        formula = kpi.formula

        # Stores computed formula with filled values for calculation
        computed_formula = ''

        # Stores multiplier field
        multiplier = ''

        # Checks if there's a square bracket or not
        in_square_bracket = False

        # Stores current parameter ID
        current_parameter_id = ''

        if not formula:
            return

        for f in formula:
            if f in ('[', ']'):
                if f == '[':
                    in_square_bracket = True
                else:
                    in_square_bracket = False
            elif in_square_bracket:
                computed_formula = computed_formula + f
            elif f.isdigit():
                current_parameter_id = current_parameter_id + f
            else:
                if current_parameter_id != '':
                    computed_formula = str(computed_formula) + str(
                        parameters.search([('kpi', '=', kpi.id), ('kpi_status_id', '=', self.id),
                                           ('parameter', '=', int(current_parameter_id))]).value) + str(f)
                    current_parameter_id = ''
                else:
                    computed_formula = str(computed_formula) + str(f)

        if current_parameter_id != '':
            computed_formula = str(computed_formula) + str(
                parameters.search([('kpi', '=', kpi.id), ('kpi_status_id', '=', self.id),
                                   ('parameter', '=', int(current_parameter_id))]).value)

            # Calculate Result
        result = eval(computed_formula)

        # Update Calculated Result
        var = self.env['kpi.status.parameter'].search([('kpi', '=', kpi.id), ('kpi_status_id', '=', self.id)])

        var.write(
            {'result': result})

    def load_parameters(self, kpi_status_id):
        parameters = self.env['kpi.parameter'].search([])

        for parameter in parameters:
            if self.check_parameter(kpi_status_id, parameter.id):
                z = {
                    'kpi_status_id': kpi_status_id,
                    'kpi': parameter.kpi.id,
                    'parameter': parameter.id,
                    'value': 0
                }

                self.env['kpi.status.parameter'].create(z)

    def check_parameter(self, kpi_status_id, parameter_id):

        result_count = self.env['kpi.status.parameter'].search_count(
            [('kpi_status_id', '=', kpi_status_id), ('parameter', '=', parameter_id)])

        if result_count == 0:
            return True

        else:
            return False

    def check_region_city(self, region, city, year):
        result_count = self.env['kpi.status'].search_count(
            [('region', '=', region), ('city', '=', city), ('year', '=', year)])

        if result_count == 0:
            return False
        else:
            return True


class kpistatusparameter(models.Model):
    _name = 'kpi.status.parameter'

    @api.one
    @api.depends('kpi')
    def compute_kpi_name(self):
        self._cr.execute(
            "select min(id) from kpi_status_parameter where kpi=" + str(self.kpi.id) + " and kpi_status_id=" + str(
                self.kpi_status_id.id))
        kpi = self._cr.fetchone()

        for min_id in kpi:
            if min_id == self.id:
                self.kpiname = self.kpi.name
            else:
                self.kpiname = ''

    kpi_status_id = fields.Many2one('kpi.status', 'Parameters')
    kpi = fields.Many2one('kpi.kpi', required=True)
    kpiname = fields.Char(string='KPI', compute='compute_kpi_name')
    parameter = fields.Many2one('kpi.parameter', required=True)
    value = fields.Float('Value', required=True)
    result = fields.Float('Result')
