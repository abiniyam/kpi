from odoo import models, fields, api
from odoo.tools.sql import drop_view_if_exists


class kpiReport(models.Model):
    _name = 'kpi.status.report'
    _auto = False

    id = fields.Integer('Id')
    region = fields.Many2one('kpi.regions', string='Region')
    city = fields.Many2one('kpi.city', string='City')
    year = fields.Char('Year')
    kpi = fields.Many2one('kpi.kpi', string='KPI')
    result = fields.Float('Result')

    @api.model_cr
    def init(self):
        drop_view_if_exists(self.env.cr, 'kpi_status_report')
        self.env.cr.execute("""
                      create or replace view kpi_status_report as (
                         select row_number() over() as id,x.region,x.city,x.year,x.kpi,x.result  from(
                            select distinct a.region,a.city,a.year,b.kpi,coalesce(b.result,0) as result 
                            from kpi_status a inner join kpi_status_parameter b on a.id=b.kpi_status_id)x                
                      )""")
